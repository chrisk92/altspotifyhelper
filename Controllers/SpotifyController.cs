﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Web.Http;
using AltSpotifyApi.Models;
using com.spotify.connectstate;
using xyz.gianlu.librespot.common;
using xyz.gianlu.librespot.core;

namespace AltSpotifyApi.Controllers
{
    public class SpotifyController : ApiController
    {
        // POST api/spotify
        public string Post([FromBody] SpotifyAuthentication auth)
        {
            try
            {
                Session.Configuration conf = new Session.Configuration.Builder()
                    .setCacheEnabled(false)
                    .setStoreCredentials(false)
                    .build();

                Session.Builder builder = (Session.Builder) new Session.Builder(conf)
                    .setPreferredLocale("JP")
                    .setDeviceType(Connect.DeviceType.COMPUTER)
                    .setDeviceName("AltSpotify")
                    .setDeviceId(Utils.randomHexString(new java.util.Random(), 40));

                builder.userPass(auth.Username, auth.Password);

                Session session = builder.create();
                session.mercury();
                var token = session.tokens().getToken("user-library-read").accessToken;
                return token;
            }
            catch (Exception x)
            {
                throw new HttpResponseException(HttpStatusCode.InternalServerError);
            }
        }
    }
}
