﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AltSpotifyApi.Models
{
    public class SpotifyAuthentication
    {
        public string Username { get; set; }
        public string Password { get; set; }
    }
}